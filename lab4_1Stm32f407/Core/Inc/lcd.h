/*
 * lcd.h
 *
 *  Created on: Oct 12, 2021
 *      Author: Taras
 */


#ifndef LCD_H
#define LCD_H
#include "stm32f4xx_hal.h"
#define lcdPort  GPIOE
#define rsPin  GPIO_PIN_7
#define rwPin  GPIO_PIN_10
#define enPin  GPIO_PIN_11
#define dh4Pin  GPIO_PIN_12
#define dh5Pin  GPIO_PIN_13
#define dh6Pin  GPIO_PIN_14
#define dh7Pin  GPIO_PIN_15
#define DISPLAYPARAM 0x04
#define CURSORPARAM 0x02
#define BLINKPARAM 0x01
#define INCREMENTNODE 0x02
#define SCROLLINC 0x01
#define SCREENCLEAR 0x01
#define RIGHTSHIFT 0x04
#define DISPLAYSHIFT 0x08
#define LEFTSHIFT 0x00
#define CURSORRETURN 0x02
#define DISPLAYLENGTH 16
void LcdInit();
void RS(uint8_t x);
void RW(uint8_t x);
void EN(uint8_t x);
void DH4(uint8_t x);
void DH5(uint8_t x);
void DH6(uint8_t x);
void DH7(uint8_t x);
void lcdSend(uint8_t isCommand,uint8_t data);
void lcdCommand(uint8_t cmd);
void lcdChar(const char chr);
void setCursor(uint8_t col, uint8_t row);
void parametrsToggle(uint8_t display,uint8_t cursor,uint8_t blink);
void shiftToggle(uint8_t rightShift,uint8_t scrollInc);
void scrollRight(void);
void scrollLeft(void);
void clear(void);
void startPosition(void);
void lcdString(char* str);


#endif
