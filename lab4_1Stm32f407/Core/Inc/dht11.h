/*
 * dht11.h
 *
 *  Created on: Oct 12, 2021
 *      Author: Taras
 */

#ifndef INC_DHT11_H_
#define INC_DHT11_H_
#include "stm32f4xx_hal.h"
#define DHT11_PORT GPIOE
#define DHT_MAX_US 200
#define DHT11_PIN GPIO_PIN_5
#define DHT_TIMEOUT 				10000
#define DHT_POLLING_CONTROL			1
#define DHT_POLLING_INTERVAL_DHT11	2000
uint8_t DHT11Start(void);
uint8_t DHT11Read(void);
void SetPinOutput(GPIO_TypeDef *GPIOx,uint16_t GPIOPin);
void SetPinInput(GPIO_TypeDef *GPIOx,uint16_t GPIOPin);

#endif /* INC_DHT11_H_ */
