/*
 * lcd.c
 *
 *  Created on: Oct 12, 2021
 *      Author: Taras
 */
#include "lcd.h"
LcdInit(){
	HAL_Delay(40);
	HAL_Delay(40);
	lcdCommand(0x02);
	lcdCommand(0x28);
	lcdCommand(0x28);
	parametrsToggle(1,0,0);
	HAL_Delay(15);
	clear();
	shiftToggle(1,0);
}
void RS(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, rsPin, x);
}
void RW(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, rwPin, x);
}
void EN(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, enPin, x);
}
void DH4(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, dh4Pin, x);
}
void DH5(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, dh5Pin, x);
}
void DH6(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, dh6Pin, x);
}
void DH7(uint8_t x){
	HAL_GPIO_WritePin(lcdPort, dh7Pin, x);
}
void lcdSend(uint8_t isCommand,uint8_t data){
	RS(isCommand==0);
	HAL_Delay(1);
	DH7(((data>>7) & 1)==1);
	DH6(((data>>6) & 1)==1);
	DH5(((data>>5) & 1)==1);
	DH4(((data>>4) & 1)==1);
	EN(1);
	HAL_Delay(1);
	EN(0);
	DH7(((data>>3)&1)==1);
	DH6(((data>>2)&1)==1);
	DH5(((data>>1)&1)==1);
	DH4((data&1)==1);
	EN(1);
	HAL_Delay(1);
	EN(0);
}
void lcdCommand(uint8_t cmd) {
    lcdSend(1, cmd);
    HAL_Delay(1);
}
void lcdChar(const char chr) {
    lcdSend(0, (uint8_t)chr);
}
void setCursor(uint8_t col, uint8_t row){

	lcdSend(1,0x80|(col|row*0x40));
}
void parametrsToggle(uint8_t display,uint8_t cursor,uint8_t blink){
	uint8_t comm=0x08; //0000001000
	if(display) comm|=DISPLAYPARAM ;//0000001100
	if(cursor) comm|=CURSORPARAM; //0000001010
	if(blink) comm|=BLINKPARAM; //0000001001
	lcdCommand(comm);
}
void shiftToggle(uint8_t incrementMode,uint8_t scrollInc){
	uint8_t comm= 0x04;//0000000100
	if(incrementMode)comm|=INCREMENTNODE;//0000000110
	if(scrollInc)comm|=SCROLLINC;//0000000101
	lcdCommand(comm);
}
void scrollRight(void){
	lcdCommand(0x10|DISPLAYSHIFT|LEFTSHIFT);
}
void scrollLeft(void){
	lcdCommand(0x10|DISPLAYSHIFT|RIGHTSHIFT);
}
void clear(void ){
	lcdCommand(SCREENCLEAR);
	HAL_Delay(2);
}
void startPosition(void){
	lcdCommand(CURSORRETURN);
	HAL_Delay(2);
}
void lcdString(char* str){
	for(uint8_t i=0;str[i]!='\0';++i){
		lcdChar(str[i]);
	}
}
void subString(char* source,uint8_t firstpos,char* result, uint8_t count ){
	for(int i=0;i<count;++i){
		result[i]=source[firstpos+i];
	}
	result[count]='\0';
}
