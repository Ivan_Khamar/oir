/*
 * distance_sensor.h
 *
 *  Created on: Nov 16, 2021
 *      Author: IvanPC
 */

#ifndef INC_IR_SENSOR_H_
#define INC_IR_SENSOR_H_

#include <stdio.h>
#include <stdbool.h>

#include <stm32f4xx_hal.h>

#include <filter.h>
#include <math_function.h>

#define TOO_FAR_DIST 1000

struct IrSensorStruct
{
 ADC_HandleTypeDef* objP_adc;
 math_function_2d_obj_t obj_func;
};
typedef struct IrSensorStruct ir_sensor_t;

void ir_sensor_init(ir_sensor_t* objPL_this, ADC_HandleTypeDef* objPL_adc);
uint16_t ir_sensor_read(ir_sensor_t* objPL_this);
uint16_t ir_sensor_get_distance(ir_sensor_t* objPL_this, uint16_t u16L_adc_val);

#endif /* INC_IR_SENSOR_H_ */
